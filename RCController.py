import RPi.GPIO as GPIO
from Globals import Events
import time


class Driver:
    def __init__(self):
        self.setup()
        self.pwm1 = GPIO.PWM(10, 100)
        self.pwm2 = GPIO.PWM(12, 100)
        self.pwm1.start(70)
        self.pwm2.start(70)

    def __delete__(self):
        GPIO.cleanup()

    def setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(10, GPIO.OUT)
        GPIO.setup(12, GPIO.OUT)

    def forward(self):
        self.pwm1.ChangeDutyCycle(70)
        self.pwm2.ChangeDutyCycle(70)

    def left(self):
        self.pwm1.ChangeDutyCycle(40)
        self.pwm2.ChangeDutyCycle(0)

    def right(self):
        self.pwm1.ChangeDutyCycle(0)
        self.pwm2.ChangeDutyCycle(40)

    def stop(self):
        self.pwm1.ChangeDutyCycle(0)
        self.pwm2.ChangeDutyCycle(0)

    def forwardRight(self):
        self.pwm1.ChangeDutyCycle(20)
        self.pwm2.ChangeDutyCycle(40)

    def forwardLeft(self):
        self.pwm1.ChangeDutyCycle(40)
        self.pwm2.ChangeDutyCycle(20)


class RCControl(object):

    def __init__(self):
        self.driver = Driver()

    def steer(self, prediction):
        if prediction == 2:
            self.driver.forward()
            print("Forward")
        elif prediction == 0:
            self.driver.left()
            print("Left")
        elif prediction == 1:
            self.driver.right()
            print("Right")
        else:
            self.stop()

    def stop(self):
        self.driver.stop()

    def evadeObstacle(self):
        self.stop()
        timeSteered = [0, 0]
        if Events.obstacleDirection == Events.front:
            steerStartTime = time.time()
            while Events.obstacleDirection == Events.front:
                self.steer(1)
            timeSteered[1] += time.time() - steerStartTime
        if Events.obstacleDirection == Events.right:
            steerStartTime = time.time()
            while Events.obstacleDirection == Events.front or Events.obstacleDirection == Events.right:
                self.steer(0)
            timeSteered[0] += time.time() - steerStartTime
        if Events.obstacleDirection == Events.left:
            steerStartTime = time.time()
            while Events.obstacleDirection == Events.left:
                self.steer(0)
            timeSteered[0] += time.time() - steerStartTime
        self.steer(2)
        time.sleep(2)
        self.stop()
        if timeSteered[0] > timeSteered[1]:
            self.steer(1)
            time.sleep(timeSteered[0] - timeSteered[1])
            self.stop()
        else:
            self.steer(0)
            time.sleep(timeSteered[1] - timeSteered[0])
            self.stop()
        self.steer(2)
        time.sleep(2)
        self.stop()
