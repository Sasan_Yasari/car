first run *picamCalib.py* for camera calibration

**Collect training data and testing data:**
First run *“collect_training_data.py”*
and then run *“stream_client.py”* on raspberry pi.
User presses keyboard to drive the RC car, frames are saved only when there is a key press action.
When finished driving, press “q” to exit, data is saved as a npz file.

then run Driver.py for driver and ultrasonic sensors
and finally run StreamClient.py for camera
