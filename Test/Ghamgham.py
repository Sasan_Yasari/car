import RPi.GPIO as GPIO
from Globals import Events
import time
import numpy as np
import pygame
from pygame.locals import *


class Driver:
    def __init__(self):
        self.setup()
        self.pwm1 = GPIO.PWM(7, 100)
        self.pwm2 = GPIO.PWM(22, 100)
        self.pwm1.start(0)
        self.pwm2.start(0)

    def __delete__(self):
        GPIO.cleanup()

    def setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(7, GPIO.OUT)
        GPIO.setup(22, GPIO.OUT)

    def forward(self):
        self.pwm1.ChangeDutyCycle(70)
        self.pwm2.ChangeDutyCycle(70)

    def left(self):
        self.pwm1.ChangeDutyCycle(40)
        self.pwm2.ChangeDutyCycle(0)

    def right(self):
        self.pwm1.ChangeDutyCycle(0)
        self.pwm2.ChangeDutyCycle(40)

    def stop(self):
        self.pwm1.ChangeDutyCycle(0)
        self.pwm2.ChangeDutyCycle(0)

    def forwardRight(self):
        self.pwm1.ChangeDutyCycle(20)
        self.pwm2.ChangeDutyCycle(40)

    def forwardLeft(self):
        self.pwm1.ChangeDutyCycle(40)
        self.pwm2.ChangeDutyCycle(20)


class CollectTrainingData(object):
    def __init__(self):
        # setup car controller
        self.controller = Driver()

        pygame.init()
        size = width, height = 100, 100
        self.screen = pygame.display.set_mode(size)
        self.collect_image()

    def collect_image(self):
        try:
            print ("Press key to ghamgham")
            while True:
                # get input from human driver
                for event in pygame.event.get():
                    print ("Got ghamgham event")
                    if event.type == KEYDOWN:
                        key_input = pygame.key.get_pressed()
                        print ("Key got pressed")
                        # complex orders
                        if key_input[pygame.K_UP] and key_input[pygame.K_RIGHT]:
                            print("Forward Right")
                            self.controller.forwardRight()
                        elif key_input[pygame.K_UP] and key_input[pygame.K_LEFT]:
                            print("Forward Left")
                            self.controller.forwardLeft()
                        # simple orders
                        elif key_input[pygame.K_UP]:
                            print("Forward")
                            self.controller.forward()
                        elif key_input[pygame.K_RIGHT]:
                            print("Right")
                            self.controller.right()
                        elif key_input[pygame.K_LEFT]:
                            print("Left")
                            self.controller.left()
                        elif key_input[pygame.K_x] or key_input[pygame.K_q]:
                            print ('exit')
                            break
                    elif event.type == pygame.KEYUP:
                        self.controller.stop()
                pygame.display.flip()
                pygame.event.pump()  # process event queue
        finally:
            print 'end'


if __name__ == '__main__':
    CollectTrainingData()

