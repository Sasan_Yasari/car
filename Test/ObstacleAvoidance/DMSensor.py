from Globals import DistanceValues
from Globals import Events
import time
import threading
import RPi.GPIO as GPIO


class DMSensor(threading.Thread):
    def __init__(self, sensorNum, triggPort, echoPort):
        threading.Thread.__init__(self)
        GPIO.setwarnings(False)
        self.GPIO_TRIGGER = triggPort
        self.GPIO_ECHO = echoPort

        self.number = sensorNum

    def setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)
        GPIO.output(self.GPIO_TRIGGER, False)

    def measure(self):
        self.setup()
        GPIO.output(self.GPIO_TRIGGER, True)
        time.sleep(0.00001)
        GPIO.output(self.GPIO_TRIGGER, False)
        echoState = 0
        while echoState == 0:
            echoState = GPIO.input(self.GPIO_ECHO)
            start = time.time()
        echoState = 1
        while echoState == 1:
            echoState = GPIO.input(self.GPIO_ECHO)
            stop = time.time()
        elapsed = stop - start
        distance = (elapsed * 34300) / 2
        return distance

    def run(self):
        try:
            while True:
                distance = self.measure()
                # print "sensor number" + str(self.number) + " measured distance %.1f cm" % distance
                if self.number == Events.right:
                    DistanceValues.distance1 = distance
                elif self.number == Events.front:
                    DistanceValues.distance2 = distance
                else:
                    DistanceValues.distance3 = distance
                time.sleep(0.005)
        finally:
            print "sensor number" + str(self.number) + " shutting down"


class ObstacleDetector(threading.Thread):
    def __init__(self, minDistance):
        threading.Thread.__init__(self)
        self.dmSensor1 = DMSensor(0, 16, 18)
        self.dmSensor2 = DMSensor(1, 13, 15)
        self.dmSensor3 = DMSensor(2, 25, 26)
        self.minDistance = minDistance

    def run(self):
        # right
        self.dmSensor1.start()
        # front
        self.dmSensor2.start()
        # left
        self.dmSensor3.start()
        try:
            while True:
                if DistanceValues.distance1 <= self.minDistance:
                    Events.obstacleDirection = Events.right
                    Events.obstacleDetected = True
                elif DistanceValues.distance2 <= self.minDistance:
                    Events.obstacleDirection = Events.front
                    Events.obstacleDetected = True
                elif DistanceValues.distance3 <= self.minDistance:
                    Events.obstacleDirection = Events.left
                    Events.obstacleDetected = True
                else:
                    Events.obstacleDetected = False
        finally:
            print "ObstacleDetector shutting down"
